@extends('layouts/admin')


@section('content')

    @if (session()->has('deleted_user'))
      <p class="bg-danger">{{session('deleted_user')}}</p>
    @endif

    @if (session()->has('create_user'))
      <p class="bg-success">{{session('create_user')}}</p>
    @endif

    @if (session()->has('update_user'))
    <p class="bg-success">{{session('update_user')}}</p>
  @endif
    <h1>User</h1>

    <table class="table table-striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Is Active</th>
            <th>Register</th>
          </tr>
        </thead>
        <tbody>
            @if ($users)
                @foreach ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td><img height="30" src="{{$user->photo ? $user->photo->file : "/images/400.png"}}" alt="not exist"></td>
                    <td><a href="users/{{$user->id}}/edit">{{($user->role->name =="admin" ? "tuan $user->name" : ($user->role->name =="author" ? "penulis $user->name" : "subs $user->name" ))}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{strtoupper($user->role->name)}}</td>
                    <td>{{$user->is_active == 1 ? 'Active':'Not Active'}}</td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                </tr>
                @endforeach
            @endif
          
         
        </tbody>
      </table>
@endsection