@extends('layouts/admin')

@section('content')
    <h1>Edit</h1>
    
    <div class="col-sm-3">
        <img height="50" class="img-responsive img-rounded" src="{{$user->photo ? $user->photo->file : "/images/400.png"}}" alt="">
    </div>

    
    <div class="col-sm-9">
        @include('includes.form_error_message')
    <form class="form-horizontal" action="{{route('users.update',['id'=>$user->id])}}" method="POST" enctype="multipart/form-data">
        @csrf
         @method('PUT')

        <div class="form-group">
          <label class="control-label col-sm-2" for="name">Name</label>
          <div class="col-sm-10">
          <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{$user->name}}">
          </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{$user->email}}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password">Password</label>
            <div class="col-sm-10">
                <input type="Password" class="form-control" id="password" placeholder="Enter password" name="password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="file">User Image</label>
            <input class="col-sm-10" type="file" class="form-control-file" id="file" name="photo_id">
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="role">Role</label>
            <select class="form-control col-sm-10" id="option" name="role_id">
                @foreach ($roles as $role)
                <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
        </div>
    
        <div class="form-group">
            <label class="control-label col-sm-2" for="is_active">Status</label>
            <div class="col-sm-10"> 
                <label class="radio-inline ">
                    <input type="radio" name="is_active" value="1" checked>Active
                </label>
                <label class="radio-inline">
                    <input type="radio" name="is_active" value="0">Not Active
                </label>
            </div>
        </div>

        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success btn-lg">Update User</button>
            
          </div>
        </div>
      </form>
      <form action="{{route('users.destroy',[$user->id])}}" method="POST">
        @csrf
        @method('delete')
             <button id="form-delete" type="submit" class="btn btn-danger btn-lg" value="DELETE">Delete User</button>
       </form>
    
    </div>
@endsection