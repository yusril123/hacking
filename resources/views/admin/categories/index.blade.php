@extends('layouts/admin')

@section('content')
    <h1>Categories</h1>
   <div class="col-sm-6">
    <form class="form-horizontal" action="{{route('category.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label class="control-label col-sm-2" for="name">Nama Kategori</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="name" placeholder="Nama Kategori" name="name" value="{{old('name')}}">
            </div>
        </div>
        <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary btn-lg">Create Category</button>
            </div>
          </div>
    </form>
   </div>
   <div class="col-sm-6">
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Kategori</th>
                <th>Dibuat pada</th>
            </tr>
        </thead>
        <tbody>
            @if ($cats)
                @foreach ($cats as $cat)
                    <tr>
                        <th>{{$cat->id}}</th>
                        <th><a href="{{route('category.edit',['id'=>$cat->id])}}">{{$cat->name}}</a></th>
                        <th>{{$cat->created_at ? $cat->created_at->diffForHumans() : "none"}}</th>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
   </div>
    
@endsection