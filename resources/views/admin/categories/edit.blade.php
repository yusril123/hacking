@extends('layouts/admin')

@section('content')
    <h1>Edit Categories</h1>
   

<form class="form-horizontal" action="{{route('category.update',['id'=>$cat->id])}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Nama Kategori</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="name" placeholder="Nama Kategori" name="name" value="{{$cat->name}}">
        </div>
    </div>
    <div class="form-group">        
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary btn-lg">Update Category</button>
            
        </div>
    </div>
</form>
<form action="{{route('category.destroy',['id'=>$cat->id])}}" method="POST">
        @csrf
        @method("DELETE")

            <button id="form-delete" type="submit" class="btn btn-danger btn-lg" value="DELETE">delete Category</button>

    </form>
    
@endsection