@extends('layouts/admin')

@section('content')
    <h1>All Comments</h1>
    @if (count($comments) > 0)
    <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Comments</th>
                    <th>Created at</th>
                    <th>View Post</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($comments as $comment)
                <tr>
                    <th>{{$comment->id}}</th>
                    <th>{{$comment->body}}</th>
                    <th>{{$comment->created_at->diffForHumans()}}</th>
                    <th><a href="{{route('home.post',$comment->id)}}">View Post</a></th>
                    @if ($comment->is_active == 0)
                        <th>
                            <form action="{{route('comments.update',['id'=>$comment->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="is_active" value="1">
                                <button class="btn btn-primary">Approve</button>
                            </form>
                        </th>
                    @else
                        <th>
                            <form action="{{route('comments.update',['id'=>$comment->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="is_active" value="0">
                                <button class="btn btn-warning">Un-Approve</button>
                            </form>
                        </th>
                    @endif
                        <th>
                            <form action="{{route('comments.destroy',['id'=>$comment->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" value="DELETE">Delete</button>
                            </form>
                        </th>
                </tr>
                @endforeach

            </tbody>
        </table>
        @else
        <h1 class="text-center">No Comments</h1>
    @endif
    
    
@endsection