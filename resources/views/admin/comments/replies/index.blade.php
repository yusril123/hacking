@extends('layouts.admin')

@section('content')
    <h1>Reply Comment</h1>
    <h6>{{$comment->body}}</h6>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Author</th>
                <th>Reply</th>
                <th>Aprrovement</th>
                <th>Created at</th>
            </tr>
        </thead>
        <tbody>
            @if ($replies)
                @foreach ($replies as $reply)
                    <tr>
                        <th>{{$reply->id}}</th>
                        <th>{{$reply->author}}</th>
                        <th>{{$reply->body}}</th>
                        <th>
                            @if ($reply->is_active == 1)
                                <form action="{{route('replies.update',['id'=>$reply->id])}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-warning" name="is_active" value="0">Un-Approve</button>
                                </form>
                                @else
                                <form action="{{route('replies.update',['id'=>$reply->id])}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-primary" name="is_active" value="1">Approve</button>
                                </form>
                            @endif
                        </th>
                        <th>{{$reply->created_at->diffForHumans()}}</th>
                        <th>
                            <form action="{{route('replies.destroy',['id'=>$reply->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" value="DELETE">Delete</button>
                            </form>
                        </th>
                    </tr>
                @endforeach
            @endif
           
        </tbody>
    </table>
@endsection