@extends('layouts/admin')

@section('content')
    <h1>Edit Post</h1>
    <div class="row">
      <div class="col-sm-3">
        <img class="img-responsive img-rounded" height="50" src="{{$post->photo ? $post->photo->file : "/images/400.png" }}" alt="">
      </div>
      <div class="col-sm-9">
      @include('includes.form_error_message')
      <form class="form-horizontal" action="{{route('posts.update',['id'=>$post->id])}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label class="control-label col-sm-2" for="title">Title</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{$post->title}}">
            </div>
          </div>
          <div class="form-group">
                  <label class="control-label col-sm-2" for="category">Category</label>
                  <select class="form-control col-sm-10" id="option" name="category_id">
                      @foreach ($cats as $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                      @endforeach
                  </select>
              </div>
          <div class="form-group">
              <label class="control-label col-sm-2" for="file">User Image</label>
          <input class="col-sm-10" type="file" class="form-control-file" id="file" name="photo_id">
          </div>
          <div class="form-group">
              <label class="control-label col-sm-2" for="body">Description</label>
              <div class="col-sm-10">
              <textarea class="form-control" name="body" id="body" cols="30" rows="10">{{$post->body}}</textarea>
              </div>
              
          </div>

          <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary btn-lg">update Post</button>
            </div>
          </div>
          
        </form>
        <form action="{{route('posts.destroy',['id'=>$post->id])}}" method="POST">
            @csrf
            @method('DELETE')
            <button id="form-delete" type="submit" class="btn btn-danger btn-lg" value="DELETE">Delete Post</button>
          </form>
      </div>
    </div>


    

    
@endsection