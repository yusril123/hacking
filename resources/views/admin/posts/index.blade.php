@extends('layouts/admin')

@section('content')
    <h1>posts</h1>

    <table class="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nama</th>
                <th>Category Id</th>
                <th>Photo</th>
                <th>Title</th>
                <th>Body</th>
                <th>Created at</th>
                <th>Update at</th>
              </tr>
            </thead>
            <tbody>
                @if ($posts)
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{$post->id}}</td>
                            <td>{{$post->user->name}}</td>
                            <td>{{$post->category ? $post->category->name : 'belum terkategori'}}</td>
                            <td><img height="50" src="{{$post->photo ? $post->photo->file : "/images/400.png"}}" alt=""></td>
                            <td><a href="posts/{{$post->id}}/edit">{{$post->title}}</a></td>
                            <td>{{Str::limit($post->body,5)}}</td>
                            <td>{{$post->created_at->diffForHumans()}}</td>
                            <td>{{$post->updated_at->diffForHumans()}}</td>
                            <td><a href="{{route('home.post',['id'=>$post->slug])}}">view post</a></td>
                            <td><a href="{{route('comments.show',['id'=>$post->id])}}">view comment</a></td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
          </table>
          <div class="text-center">
            
             {{$posts->render()}}
          </div>
@endsection