@extends('layouts/admin')

@section('content')
    <h1>create posts</h1>
    @include('includes.tinyeditor')
    @include('includes.form_error_message')
    <form class="form-horizontal" action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label class="control-label col-sm-2" for="title">Title</label>
          <div class="col-sm-10">
          <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{old('title')}}">
          </div>
        </div>
        <div class="form-group">
                <label class="control-label col-sm-2" for="category">Category</label>
                <select class="form-control col-sm-10" id="option" name="category_id">
                    @foreach ($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                </select>
            </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="file">User Image</label>
            <input class="col-sm-10" type="file" class="form-control-file" id="file" name="photo_id">
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="body">Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="body" id="body" cols="30" rows="10"></textarea>
            </div>
            
        </div>

        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-lg">Create Post</button>
          </div>
        </div>
      </form>
@endsection