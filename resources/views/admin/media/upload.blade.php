@extends('layouts/admin')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
@endsection
@section('content')
    <h1>UPLOAD MEDIA</h1>

    <form class="dropzone" action="{{route('media.store')}}" method="post">
    @csrf
    @method('POST')

    </form>


@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
@endsection