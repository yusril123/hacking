@extends('layouts/admin')

@section('content')
    <h1>MEDIA</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created_at</th>
                <th>Owner</th>
                <th>Action</th>
            </tr>
        </thead>
        @if ($medias)
            @foreach ($medias as $media)
                <tr>
                    <th>{{$media->id}}</th> 
                    <th><img width="100" src="{{$media->file ? $media->file : "images/404.png"}}" alt=""></th> 
                    <th>{{$media->created_at->diffForhumans()}}</th> 
                    <th></th>
                    <th>
                        <form action="{{route('media.destroy',['id'=>$media->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button value="DELETE" class="btn btn-danger">Delete</button>
                        </form>
                    </th>
                </tr> 
            @endforeach
        @endif
            
        <tbody>

        </tbody>
    </table>
@endsection