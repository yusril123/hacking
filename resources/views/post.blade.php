@extends('layouts.blog-post')

@section('content')
    <!-- Blog Post -->

    <!-- Title -->
    <h1>{{Str::title($post->title)}}</h1>

    <!-- Author -->
    <p class="lead">
        by <a href="#">{{$post->user->name}}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span> Posted on {{$post->created_at->isoFormat('MMMM Do YYYY, h:mm:ss a')}}</p>

    <hr>

    <!-- Preview Image -->
    <img class="img-responsive" src="{{$post->photo ? $post->photo->file : "images/400.png"}}" alt="">

    <hr>

    <!-- Post Content -->
    
    
    <p>{!!$post->body!!}</p>
    <hr>

    <!-- Blog Comments -->
    @if (Auth::check())
    <!-- Comments Form -->
    <div class="well">
        @if (Session::has('comment'))
            {{Session('comment')}}
        @endif
        <h4>Leave a Comment:</h4>
    <form action="{{route('comments.store')}}" method="POST">
            @csrf
            @method('POST')
            <input type="hidden" name="post_id" value="{{$post->id}}">
            <div class="form-group">
                <textarea class="form-control" rows="3" name="body"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit Comment</button>
        </form>
    </div>
    @else
    <div id="comment-btn">
        <a href="/login">login for comment</a>
    </div>
    @endif
    <hr>

    <!-- Posted Comments -->
    @if (Session::has('commentReply'))
         {{Session('commentReply')}}
    @endif
    <!-- Comment -->
    @if ($comments)
        @foreach ($comments as $comment)
            <div class="media">
                <a class="pull-left" href="#">
                    <img height="50" class="media-object" src="{{$comment->photo ? $comment->photo : "http://placehold.it/64x64"}}" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">{{$comment->author}}
                        <small>{{$comment->created_at->diffForHumans()}}</small>
                    </h4>
                    {{$comment->body}}
                        @foreach ($comment->replies as $reply)
                            {{-- is active true --}}
                            @if ($reply->is_active == 1)
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img height="50" class="media-object" src="{{$reply->photo}}" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$reply->author}}
                                        <small>{{$reply->created_at->diffForHumans()}}</small>
                                    </h4>
                                    {{$reply->body}}
                                </div>   
                            </div>
                            @endif
                             {{--end is active true --}}
                        @endforeach
                       
                        <div class="comment-reply-container">
                            <button class="toggle-reply btn btn-primary pull-right">Reply</button>
                            <div class="comment-reply">
                                <form class="form-group row" action="{{route('comment.reply',['id'=>$comment->id])}}" method="post">
                                    @csrf
                                    <input type="hidden" name="comment_id" value="{{$comment->id}}">
        
                                        <div class="col-xs-5 col-md-9 col-lg-8">
                                            <input type="text" class="form-control" id="body" placeholder="Enter Reply" name="body">
                                        </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary">Submit</button>
                                    </div>   
                                    
                                </form>
                            </div>
                        </div>
                        
                </div>
            </div>
        @endforeach
    @endif
    

@endsection

@section('category')
<div class="row">
    <div class="col-lg-6">
        <ul class="list-unstyled">
            <li><a href="#">{{Str::title($post->category->name)}}</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
        </ul>
    </div>
    <div class="col-lg-6">
        <ul class="list-unstyled">
            <li><a href="#">Category Name</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
            <li><a href="#">Category Name</a>
            </li>
        </ul>
    </div>
</div>
@endsection


@section('scripts')
    <script>
        $(".comment-reply-container .toggle-reply").click(function(){
            $(this).next().slideToggle("slow");
        })
    </script>
@endsection