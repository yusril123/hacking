@extends('layouts/admin')

@section('content')
    <div class="container text-center">
        <h1>404 | Page Not Found</h1>
        <img height="200" src="/images/sad.png" alt="">
    </div>
@endsection