<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Photo;
use App\Category;
use App\Comment;
use Auth;
use App\Http\Requests\PostRequest;
use App\Http\Requests\PostEditRequest;
class AdminPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $posts = Post::paginate(2);
        return view('admin.posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::all();
        return view('admin.posts.create',compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $input = $request->all();
        // $user = Auth::user();

        if ($file = $request->file('photo_id')) {
            $name = time(). $file->getClientOriginalName();
            $file->move('images',$name);
            $photo = Photo::create(['file'=>$name]);
            $input['photo_id']=$photo->id;
        }
        Auth::user()->posts()->create($input);
        // $user->posts()->create($input);
        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cats = Category::all();

        $post = Post::findOrFail($id);
        return view('admin.posts.edit',compact('cats','post'));


        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostEditRequest $request, $id)
    {
        // $post = Post::findOrFail($id);
        $input = $request->all();
        if ($file = $request->file('photo_id')) {
            $name = time(). $file->getClientOriginalName();
            $file->move('images',$name);

            $photo =Photo::create(['file'=>$name]);
            $input['photo_id']=$photo->id;
        }
        // $post->update($input);
        Auth::User()->posts()->whereId($id)->first()->update($input);
        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::findOrFail($id);
        unlink(public_path().$post->photo->file);
        $post->delete($id);

        return redirect('admin/posts');
    }

    public function post($id){
        $post = Post::where('slug',$id)->firstOrFail();
        $comments = $post->comments()->whereIsActive(1)->get();
       
        return view('post',compact('post','comments'));
    }
}
