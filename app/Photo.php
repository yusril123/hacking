<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $directory='/images/';
    protected $fillable = [
        'file',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function getFileAttribute($photo){
        return $this->directory . $photo;
    }
}
